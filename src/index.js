const Pipeline = require('./Pipeline').Pipeline
const config = require('../config.json')

if (!config) {
  console.error('Config not found!')
  process.exit(1)
}

const pipeline = new Pipeline(config)

// check level and flow every X seconds and update central
setInterval(pipeline.updateCentral.bind(pipeline), config.notificationFrequency)
