const Gpio = require('onoff').Gpio
const { exec } = require('child-process-promise')
const axios = require('axios').default

class Pipeline {

    constructor (config) {
        this.notified = false
        this.config = config
        this.initLevelPins().then()
    }

    async initLevelPins () {
        this.lvl50 = new Gpio(this.config.level.halfPin, 'in', 'rising')
        this.lvl75 = new Gpio(this.config.level.quarterPin, 'in', 'rising')
        this.lvl100 = new Gpio(this.config.level.fullPin, 'in', 'rising')

        // lib doesn't support setting mode as pullup/down AFAIK, doing it manually
        const command = `
            gpio -g mode ${this.config.level.halfPin} up &&
            gpio -g mode ${this.config.level.quarterPin} up &&
            gpio -g mode ${this.config.level.fullPin} up
        `

        console.log('Setting up pin modes...')

        try {
            await exec(command)
            console.log('Done.')
        } catch (err) {
            console.error(err)
            process.exit(1)
        }
    }

    async readLevel () {
        let level = -1

        const value50 = await this.lvl50.read()
        const value75 = await this.lvl75.read()
        const value100 = await this.lvl100.read()

        // values are inverted (1 means water is not there)
        if (!value50 && !value75 && !value100) {
          level = 100
        } else if (!value50 && !value75 && value100) {
          level = 75
        } else if (!value50 && value75 && value100) {
          level = 50
        } else if (value50 && value75 && value100) {
          level = 0;
        }

        return level
    }

    async getFlow () {
        const command = this.config.flowCommand
        const flow = await exec(command)
        return flow.stdout
    }

    async updateCentral () {
        const level = await this.readLevel()
        const flow = await this.getFlow()
        const timestamp = Date.now()
        await axios.post(this.config.centralUrl, { level, flow, timestamp })
    }
}

module.exports.Pipeline = Pipeline
