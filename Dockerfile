FROM node:10-alpine

COPY . /backend

WORKDIR /backend

RUN npm i

ENTRYPOINT ["node", "src"]

